<?php include('server.php');
if(isset($_SESSION["Username"])){
	$username=$_SESSION["Username"];
}
else{
    $username="";
	//header("location: index.php");
}

$sql = "SELECT * FROM klien WHERE username='$username'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $name=$row["Name"];
        $email=$row["email"];
        $contactNo=$row["contact_no"];
        $gender=$row["gender"];
        $birthdate=$row["birthdate"];
        $address=$row["address"];
        $profile_sum=$row["profile_sum"];
        $company=$row["company"];
        $password=$row["password"];
        }
} else {
    echo "0 results";
}


if(isset($_POST["editEmployer"])){
    $name=test_input($_POST["name"]);
    $email=test_input($_POST["email"]);
    $contactNo=test_input($_POST["contactNo"]);
    $gender=test_input($_POST["gender"]);
    $birthdate=test_input($_POST["birthdate"]);
    $address=test_input($_POST["address"]);
    $profile_sum=test_input($_POST["profile_sum"]);
    $company=test_input($_POST["company"]);
    $password=test_input($_POST["password"]);
    $image = $_FILES['image']['name'];
    $sql = "INSERT INTO pekerja (image) VALUES ('$image')";
    mysqli_query($conn, $sql);
    move_uploaded_file($_FILES['image']['tmp_name'], "gambar/".$_FILES['image']['name']);
    $result = mysqli_query($conn, "SELECT * FROM klien");



    $sql = "UPDATE klien SET Name='$name',password='$password',email='$email',contact_no='$contactNo', address='$address', gender='$gender', profile_sum='$profile_sum', birthdate='$birthdate', company='$company' WHERE username='$username'";

    
    $result = $conn->query($sql);
    if($result==true){
        header("location: employerProfile.php");
    }
}


 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Edit Profil Klien - Uptable</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="awesome/css/fontawesome-all.min.css">
	<link rel="stylesheet" type="text/css" href="dist/css/bootstrapValidator.css">

<style>
	body{padding-top: 3%;margin: 0;}
	.card{box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); background:#fff}
</style>

</head>
<body>

<!--Navbar menu-->
<nav class="navbar navbar-inverse navbar-fixed-top" id="my-navbar">
	<div class="container">
		<div class="navber-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="index.php" class="navbar-brand">UpTable</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="allJob.php">Semua Projek</a></li>
				<li><a href="allFreelancer.php">Cari Pekerja</a></li>
				<li><a href="klien.php">Cari Klien</a></li>
				<li class="dropdown" style="background:#000;padding:0 20px 0 20px;">
			        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> <?php echo $username; ?>
			        </a>
			        <ul class="dropdown-menu list-group list-group-item-info">
			        	<a href="employerProfile.php" class="list-group-item"><span class="glyphicon glyphicon-home"></span>  Profil</a>
			          	<a href="editEmployer.php" class="list-group-item"><span class="glyphicon glyphicon-inbox"></span>  Edit Profil</a>
					  	<a href="message.php" class="list-group-item"><span class="glyphicon glyphicon-envelope"></span>  Pesan</a> 
					  	<a href="logout.php" class="list-group-item"><span class="glyphicon glyphicon-ok"></span>  Keluar</a>
			        </ul>
			    </li>
			</ul>
		</div>		
	</div>	
</nav>
<!--End Navbar menu-->


<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="page-header">
                    <h2>Edit Profil</h2>
                </div>

                <form id="registrationForm" method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" value="<?php echo $name; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Alamat email</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" value="<?php echo $email; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">No. HP</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="contactNo" value="<?php echo $contactNo; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Jenis Kelamin</label>
                    <div class="col-sm-5">
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" 
                                <?php if (isset($gender) && $gender=="male") echo "checked";?>
                                 value="male" /> Laki-laki
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" 
                                <?php if (isset($gender) && $gender=="female") echo "checked";?>
                                 value="female" /> Perempuan
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" 
                                <?php if (isset($gender) && $gender=="Lainnya") echo "checked";?>
                                 value="other" /> Other
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Tanggal Lahir</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="birthdate" placeholder="YYYY/MM/DD" value="<?php echo $birthdate; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Alamat</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" value="<?php echo $address; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama Perusahaan</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="company" value="<?php echo $company; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Ringkasan Profil</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="profile_sum" value="<?php echo $profile_sum; ?>" />
                    </div>
                </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Ganti Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" name="password" value="<?php echo $password; ?>" />
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label">Unggah Foto Profil</label>
                    <div class="col-sm-5">
                    <input type="file" class="form-control" name="image" value="<?php echo $image; ?>" />
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <!-- Do NOT use name="submit" or id="submit" for the Submit button -->
                        <button type="submit" name="editEmployer" class="btn btn-info btn-lg">Simpan Perubahan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>




<script type="text/javascript" src="jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>

<script>
$(document).ready(function() {
    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Nama tidak boleh kosong'
                    }
                }
            },
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Username tidak boleh kosong'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'Username harus lebih dari 6 dan kurang dari 30 karakter'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'Username hanya dapat terdiri dari abjad dan angka'
                    },
                    different: {
                        field: 'Password',
                        message: 'Username dan kata sandi tidak boleh sama satu sama lain'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email tidak boleh kosong'
                    },
                    emailAddress: {
                        message: 'Email tidak vaid'
                    }
                }
            },
            password: {
                    different: {
                        field: 'username',
                        message: 'Password tidak boleh sama dengan username'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Password setidaknya mengandung 6 karakter'
                    }
                }
            },
            repassword: {
                validators: {
                    notEmpty: {
                        message: 'Konfirmasi password tidak boleh kosong'
                    },
                    identical: {
                        field: 'password',
                        message: 'Password tidak cocok'
                    }
                }
            },
            contactNo: {
                validators: {
                    notEmpty: {
                        message: 'No Telepon diperlukan'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Nomor tidak valid'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'Jenis Kelamin diperlukan'
                    }
                }
            },
            birthdate: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal lahir diperlukan'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Tanggal lahir tidak valid'
                    }
                }
            },
            address: {
                validators: {
                    notEmpty: {
                        message: 'Alamat diperlukan'
                    }
                }
            },
            usertype: {
                validators: {
                    notEmpty: {
                        message: 'Tipe Pengguna diperlukan'
                    }
                }
            }
        }
    });
});
</script>

</body>
</html>