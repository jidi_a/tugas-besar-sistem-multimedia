<?php include('server.php');
if(isset($_SESSION["Username"])){
	$username=$_SESSION["Username"];
	if ($_SESSION["Usertype"]==1) {
		header("location: freelancerProfile.php");
	}
	else{
		header("location: employerProfile.php");
	}
}
else{
    $username="";
	//header("location: index.php");
}

 ?>


<!DOCTYPE html>
<html>
<head>
	<title>Uptable</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="awesome/css/fontawesome-all.min.css">

<style>
	body{padding-top: 3%;margin: 0;}
	.header1{background-color: #EEEEEE;padding-left: 1%;}
	.header2{padding:20px 40px 20px 40px;color:#fff;}
	.card{box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); background:#fff}
</style>

</head>
<body>

<!--Navbar menu-->
<nav class="navbar navbar-inverse navbar-fixed-top" id="my-navbar">
	
	<div class="container">
		<div class="navber-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="index.php" class="navbar-brand">UpTable</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<a href="loginReg.php" class="btn btn-info navbar-btn navbar-right">Daftar</a>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.php">Beranda</a></li>
				<li><a href="loginReg.php">Masuk</a></li>
			</ul>
		</div>		
	</div>	
</nav>
<!--End Navbar menu-->



<!--Header and slider-->

<!--Header-->
<div class="row header1">
	<div class="col-lg-4">
		<div class="jumbotron">
			<div class="container text-center">
				<h1>UpTable</h1>
				<p>Tawarkan dan dapatkan pekerjaan disini, tidak ada waktu lagi untuk rebahan</p>
				<a href="loginReg.php" class="btn btn-warning btn-lg">Daftar Sekarang, Gratis!</a>
				<p></p>
			</div>
		</div>	
	</div>
<!--End Header-->

<!--slider-->
	<div class="col-lg-8">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#myCarousel" data-slide-to="1"></li>
		    <li data-target="#myCarousel" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
		    <div class="item active">
		      <img src="image/computer.jpg" alt="Chania">
		      <div class="carousel-caption">
		        <!-- <h3>Work</h3>
		        <p>Work hard to be successful.</p> -->
		      </div>
		    </div>

		    <div class="item">
		      <img src="image/mug.jpg" alt="Chania">
		      <div class="carousel-caption">
		       <!--  <h3>Time</h3>
		        <p>Do not waste your time.</p> -->
		      </div>
		    </div>

		    <div class="item">
		      <img src="image/coat.jpg" alt="Flower">
		      <div class="carousel-caption">
		       <!--  <h3>Believe</h3>
		        <p>Always believe in yourself.</p> -->
		      </div>
		    </div>
		  </div>

		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</div>
</div>
<!--End slider-->
<!--End Header and slider-->





<!--Popular Categories-->
<div class="container text-center" style="padding:4%;" id="category">
	<h1 class="card header2" style="background:#007BFF">Kategori</h1>
	<div class="row">
		<div class="col-lg-4">
			<div class="card" style="padding:20px 40px 20px 40px;margin:20px;">
				<a href="loginReg.php"><span class="glyphicon glyphicon-credit-card"></span>
				<h3>Web Developer</h3>
				<p>Silakan masuk dan jelajahi Web Developer kami</p></a>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card" style="padding:20px 40px 20px 40px;margin:20px;">
				<a href="loginReg.php"><span class="glyphicon glyphicon-phone"></span>
				<h3>Mobile Developer</h3>
				<p>Silakan masuk dan jelajahi Mobile Developer kami</p></a>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card" style="padding:20px 40px 20px 40px;margin:20px;">
				<a href="loginReg.php"><span class="glyphicon glyphicon-picture"></span>
				<h3>Desainer Grafik</h3>
				<p>Silakan masuk dan jelajahi Graphic Desainer kami</p></a>
			</div>
		</div>
	</div>
		<div class="row">
		<div class="col-lg-4">
			<div class="card" style="padding:20px 40px 20px 40px;margin:20px;">
				<a href="loginReg.php"><span class="glyphicon glyphicon-pencil"></span>
				<h3>Creative writer</h3>
				<p>Silakan masuk dan jelajahi Creative Writer kami</p></a>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card" style="padding:20px 40px 20px 40px;margin:20px;">
				<a href="loginReg.php"><span class="glyphicon glyphicon-signal"></span>
				<h3>Marketing Expert</h3>
				<p>Silakan masuk dan jelajahi Marketing Expert kami</p></a>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card" style="padding:20px 40px 20px 40px;margin:20px;">
				<a href="loginReg.php"><span class="glyphicon glyphicon-headphones"></span>
				<h3>Virtual Assistant</h3>
				<p>Silakan masuk dan jelajahi Virtual Assistant kami</p></a>
			</div>
		</div>
	</div>
</div>
<!--End Popular Categories-->


<script type="text/javascript" src="jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>